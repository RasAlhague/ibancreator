﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
namespace IbanCreator.UI
{
	public static class StringExtensions
	{
		public static string RemoveSpecialCharacters(this string str)
		{
			return Regex.Replace(str, "[^a-zA-Z0-9]+", "", RegexOptions.Compiled);
		}
		public static string RemoveLetters(this string str)
		{
			for (int i = 1; i <= 26; i++)
			{
				str = str.Replace(((char)(i + 64)).ToString(), (i + 9).ToString());
			}
			return str;
		}
		public static string AddLeadingZeros(this string str, int length)
		{
			return str.ToString().PadLeft(length, '0').Substring(0, length);
		}
        public static string SegmentifyString(this string str, int numberOfElements)
        {
            int strLength = str.Length;

            for (int i = 0; i < str.Length - (i / 4); i+=4)
            {
                str = str.Insert(i +(i/4), " ");
            }

            return str;
        }
	}
}