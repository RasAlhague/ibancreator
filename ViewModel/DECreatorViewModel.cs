﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.ViewModel
{
	public sealed class DECreatorViewModel : IbanCreatorBaseViewModel
	{
		public DECreatorViewModel() : base()
		{
			CountryName = "Deutschland";
		}
		protected override object CreateBBan(Type t)
		{
			return Activator.CreateInstance(t, BIC, AccountNumber);
		}
	}
}