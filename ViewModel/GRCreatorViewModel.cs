﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.ViewModel
{
	public sealed class GRCreatorViewModel : IbanCreatorBaseViewModel
	{
		private string _branchCode;
		public string BranchCode
		{
			get
			{
				return _branchCode;
			}
			set
			{
				SetProperty<string>(ref _branchCode, value);
			}
		}
		public GRCreatorViewModel() : base()
		{
			CountryName = "Griechenland";
		}
		protected override object CreateBBan(Type t)
		{
			return Activator.CreateInstance(t, BIC, BranchCode, AccountNumber);
		}
	}
}