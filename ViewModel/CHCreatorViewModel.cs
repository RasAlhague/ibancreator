﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.ViewModel
{
	public sealed class CHCreatorViewModel : IbanCreatorBaseViewModel
	{
		public CHCreatorViewModel() : base()
		{
			CountryName = "Schweiz";
		}
		protected override object CreateBBan(Type t)
		{
			return Activator.CreateInstance(t, BIC, AccountNumber);
		}
	}
}