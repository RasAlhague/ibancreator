﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.ViewModel
{
	public sealed class FRCreatorViewModel : IbanCreatorBaseViewModel
	{
		private string _nationalCheckDigits;
		private string _branchCode;
		public string NationalCheckDigits
		{
			get
			{
				return _nationalCheckDigits;
			}
			set
			{
				SetProperty<string>(ref _nationalCheckDigits, value);
			}
		}
		public string BranchCode
		{
			get
			{
				return _branchCode;
			}
			set
			{
				SetProperty<string>(ref _branchCode, value);
			}
		}
		public FRCreatorViewModel() : base()
		{
			CountryName = "Frankreich";
		}
		protected override object CreateBBan(Type t)
		{
			return Activator.CreateInstance(t, BIC, BranchCode, AccountNumber, NationalCheckDigits);
		}
	}
}