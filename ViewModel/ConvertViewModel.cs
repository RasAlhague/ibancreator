﻿using IbanCreator.UI.Commands;
using IbanCreator.UI.Model.IBAN;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace IbanCreator.UI.ViewModel
{
    public class ConvertViewModel : ViewModelBase
    {
        public ObservableCollection<IbanBase> IbanListe { get; private set; }
        public ICommand OpenCommand { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ICommand CloseCommand { get; private set; }

        public ConvertViewModel()
        {
            IbanListe = new ObservableCollection<IbanBase>();
            OpenCommand = new RelayCommand(OpenExecute);
            SaveCommand = new RelayCommand(SaveExecute);
            CloseCommand = new RelayCommand(CloseExecute);
        }

        private void CloseExecute(object obj)
        {
            
        }

        private void SaveExecute(object obj)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (var fs = new FileStream(saveFileDialog.FileName, FileMode.Create))
                {
                    using (var sw = new StreamWriter(fs))
                    {
                        foreach (var item in IbanListe)
                        {
                            sw.WriteLine(item.Iban);
                        }
                    }
                }
            }
        }

        private void OpenExecute(object obj)
        {
            string[] fileContent = null;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "csv files (*.csv)|*.csv";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    fileContent = File.ReadAllLines(filePath);
                    CreateIbanList(fileContent);
                }
            }
        }

        private void CreateIbanList(string[] content)
        {
            IbanListe.Clear();

            foreach (var line in content.Skip(1))
            {
                var t = Type.GetType("IbanCreator.UI.Model.IBAN." + line.Split(';')[4] + "Iban");

                IbanListe.Add(Activator.CreateInstance(t, line.Split(';')) as IbanBase);
            }
        }
    }
}
