﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.ViewModel
{
	public sealed class BECreatorViewModel : IbanCreatorBaseViewModel
	{
		private string _nationalCheckDigits;
		public string NationalCheckDigits
		{
			get
			{
				return _nationalCheckDigits;
			}
			set
			{
				SetProperty<string>(ref _nationalCheckDigits, value);
			}
		}
		public BECreatorViewModel() : base()
		{
			CountryName = "Belgien";
		}
		protected override object CreateBBan(Type t)
		{
			return Activator.CreateInstance(t, BIC, AccountNumber, NationalCheckDigits);
		}
	}
}