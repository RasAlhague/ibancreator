﻿using IbanCreator.UI.Commands;
using IbanCreator.UI.Model;
using IbanCreator.UI.Model.BBAN;
using IbanCreator.UI.Model.IBAN;
using IbanCreator.UI.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
namespace IbanCreator.UI.ViewModel
{
	public class MainViewModel : ViewModelBase
	{
		private string _länderName;
		private string _inputIban;
		private bool? _isValid;
		public bool? IsValid
		{
			get
			{
				return _isValid;
			}
			set
			{
				SetProperty<bool?>(ref _isValid, value);
			}
		}
		public string LänderName
		{
			get
			{
				return _länderName;
			}
			set
			{
				SetProperty<string>(ref _länderName, value);
			}
		}
		public string InputIban
		{
			get
			{
				return _inputIban;
			}
			set
			{
				SetProperty<string>(ref _inputIban, value);
			}
		}
		public List<string> Countrys { get; private set; }
		public ICommand ValidierenCommand { get; private set; }
		public ICommand SelectedItemChangedCommand { get; private set; }
        public ICommand ConvertCommand { get; private set; }
        public ICommand ValidateIbanListCommand { get; private set; }
		public MainViewModel()
		{
			var codes = CountryCode.GetListOfCountryCodes(".\\Resources\\CountryCodes.csv");
			Countrys = codes.Select(code => code.Country).ToList();
			ValidierenCommand = new RelayCommand(ValidierenExecute);
			SelectedItemChangedCommand = new RelayCommand(SelectedItemChangedExecute);
            ConvertCommand = new RelayCommand(ConvertAccountListExecute);
            ValidateIbanListCommand = new RelayCommand(ValidateIbanListExecute);
		}

        private void ValidateIbanListExecute(object obj)
        {
            throw new NotImplementedException();
        }

        private void ConvertAccountListExecute(object obj)
        {
            try
            {
                ConvertView view = new ConvertView();
                view.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured. Program is now closing!");
                Environment.Exit(0);
            }
        }
		private void SelectedItemChangedExecute(object obj)
		{
			var uiControls = obj as UIElementCollection;
			uiControls.Clear();
			var code = CountryCode.GetListOfCountryCodes(".\\Resources\\CountryCodes.csv").FirstOrDefault(ccode => ccode.Country == LänderName).Code;
			uiControls.Add(Activator.CreateInstance(Type.GetType("IbanCreator.UI.View." + code + "CreatorView")) as UserControl);
		}
		private void ValidierenExecute(object obj)
		{
			InputIban = InputIban.Replace(" ", "");
			var iban = IbanBase.CreateIban(InputIban);
			if (iban != null && iban.BBan.ValidLength + 4 == iban.Iban.Length)
			{
				IsValid = iban.ValidateIban();
			}
			else
			{
				IsValid = false;
			}
		}
	}
}