﻿using IbanCreator.UI.Commands;
using IbanCreator.UI.Model;
using IbanCreator.UI.Model.IBAN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
namespace IbanCreator.UI.ViewModel
{
	public abstract class IbanCreatorBaseViewModel : ViewModelBase
	{
		protected string _accountNumber;
		protected string _bic;
		protected string _outputIban;
		protected string _countryName;
		public string CountryName
		{
			get
			{
				return _countryName;
			}
			set
			{
				SetProperty<string>(ref _countryName, value);
			}
		}
		public string AccountNumber
		{
			get
			{
				return _accountNumber;
			}
			set
			{
				SetProperty<string>(ref _accountNumber, value);
			}
		}
		public string BIC
		{
			get
			{
				return _bic;
			}
			set
			{
				SetProperty<string>(ref _bic, value);
			}
		}
		public string OutputIban
		{
			get
			{
				return _outputIban;
			}
			set
			{
				SetProperty<string>(ref _outputIban, value);
			}
		}
		public ICommand ErzeugenCommand { get; private set; }
		protected IbanCreatorBaseViewModel()
		{
			ErzeugenCommand = new RelayCommand(ErzeugenExecute);
		}
		protected void ErzeugenExecute(object obj)
		{
			var countryName = CountryCode.GetListOfCountryCodes(".\\Resources\\CountryCodes.csv").FirstOrDefault(code => code.Country == CountryName.Replace(" ", ""));
			if (countryName != null)
			{
				Type bBanType = Type.GetType("IbanCreator.UI.Model.BBAN." + countryName.Country + "BBan");
				var bBan = CreateBBan(bBanType);

				Type iBanType = Type.GetType("IbanCreator.UI.Model.IBAN." + countryName.Country + "Iban");
				var newIban = Activator.CreateInstance(iBanType, bBan) as IbanBase;
				OutputIban = newIban.Iban;
			}
		}
		protected abstract object CreateBBan(Type t);
	}
}