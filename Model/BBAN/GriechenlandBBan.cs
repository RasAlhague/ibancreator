﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class GriechenlandBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 23;
			}
		}
		public string BranchCode { get; set; }
		public GriechenlandBBan()
		{
		}
		public GriechenlandBBan(string bBan) : this(bBan.Substring(0, 3), bBan.Substring(3, 4), bBan.Substring(7))
		{
		}
		public GriechenlandBBan(string bic, string branchCode, string accountNumber) : base(bic, accountNumber)
		{
			BranchCode = branchCode;
		}
		protected override string AssembleBBan()
		{
			string bic = BIC.RemoveSpecialCharacters().AddLeadingZeros(3);
			string accNr = AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(16);
			string branchCode = BranchCode.RemoveSpecialCharacters().AddLeadingZeros(4);
			return bic + branchCode + accNr;
		}
	}
}