﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class GroßbritannienBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 18;
			}
		}
		public string BranchCode { get; set; }
		public GroßbritannienBBan()
		{
		}
		public GroßbritannienBBan(string bBan) : this(bBan.Substring(0, 4), bBan.Substring(4, 6), bBan.Substring(10))
		{
		}
		public GroßbritannienBBan(string bic, string branchCode, string accountNumber) : base(bic, accountNumber)
		{
			BranchCode = branchCode;
		}
		protected override string AssembleBBan()
		{
			string bic = BIC.RemoveSpecialCharacters().AddLeadingZeros(4);
			string accNr = AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(8);
			string branchCode = BranchCode.RemoveSpecialCharacters().AddLeadingZeros(6);
			return bic + branchCode + accNr;
		}
	}
}