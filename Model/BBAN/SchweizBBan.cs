﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class SchweizBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 17;
			}
		}
		public SchweizBBan()
		{
		}
		public SchweizBBan(string bBan) : this(bBan.Substring(0, 5), bBan.Substring(5))
		{
		}
		public SchweizBBan(string bic, string accountNumber) : base(bic, accountNumber)
		{
		}
		protected override string AssembleBBan()
		{
			return BIC.RemoveSpecialCharacters().AddLeadingZeros(5) + AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(12);
		}
	}
}