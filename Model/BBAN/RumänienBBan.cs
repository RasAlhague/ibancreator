﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class RumänienBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 20;
			}
		}
		public RumänienBBan()
		{
		}
		public RumänienBBan(string bBan) : this(bBan.Substring(0, 4), bBan.Substring(4))
		{
		}
		public RumänienBBan(string bic, string accountNumber) : base(bic, accountNumber)
		{
		}
		protected override string AssembleBBan()
		{
			return BIC.RemoveSpecialCharacters().AddLeadingZeros(4) + AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(16);
		}
	}
}