﻿using System;
using System.Collections.Generic;
using System.Text;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class DeutschlandBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 18;
			}
		}
		public DeutschlandBBan()
		{
		}
		public DeutschlandBBan(string bBan) : this(bBan.Substring(0, 8), bBan.Substring(8))
		{
		}
		public DeutschlandBBan(string bic, string accountNumber) : base(bic, accountNumber)
		{
		}
		protected override string AssembleBBan()
		{
			return BIC.RemoveSpecialCharacters().AddLeadingZeros(8) + AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(10);
		}
	}
}