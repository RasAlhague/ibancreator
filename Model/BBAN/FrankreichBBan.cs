﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class FrankreichBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 23;
			}
		}
		public string BranchCode { get; set; }
		public string NationalCheckDigits { get; set; }
		public FrankreichBBan()
		{
		}
		public FrankreichBBan(string bBan) : this(bBan.Substring(0, 5), bBan.Substring(5, 5), bBan.Substring(10, 11), bBan.Substring(21, 2))
		{
		}
		public FrankreichBBan(string bic, string branchCode, string accountNumber, string nationalCheckDigits) : base(bic, accountNumber)
		{
			BranchCode = branchCode;
			NationalCheckDigits = nationalCheckDigits;
		}
		protected override string AssembleBBan()
		{
			string bic = BIC.RemoveSpecialCharacters().AddLeadingZeros(5);
			string accNr = AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(11);
			string nationalCheckDigits = NationalCheckDigits.RemoveSpecialCharacters().AddLeadingZeros(2);
			string branchCode = BranchCode.RemoveSpecialCharacters().AddLeadingZeros(5);
			return bic + branchCode + accNr + nationalCheckDigits;
		}
	}
}