﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class SaudiArabienBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 20;
			}
		}
		public SaudiArabienBBan()
		{
		}
		public SaudiArabienBBan(string bBan) : this(bBan.Substring(0, 2), bBan.Substring(2))
		{
		}
		public SaudiArabienBBan(string bic, string accountNumber) : base(bic, accountNumber)
		{
		}
		protected override string AssembleBBan()
		{
			return BIC.RemoveSpecialCharacters().AddLeadingZeros(2) + AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(18);
		}
	}
}