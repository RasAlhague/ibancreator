﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class SpanienBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 20;
			}
		}
		public string BranchCode { get; set; }
		public string NationalCheckDigits { get; set; }
		public SpanienBBan()
		{
		}
		public SpanienBBan(string bBan) : this(bBan.Substring(0, 4), bBan.Substring(4, 4), bBan.Substring(8, 2), bBan.Substring(10))
		{
		}
		public SpanienBBan(string bic, string branchCode, string nationalCheckDigits, string accountNumber) : base(bic, accountNumber)
		{
			BranchCode = branchCode;
			NationalCheckDigits = nationalCheckDigits;
		}
		protected override string AssembleBBan()
		{
			string bic = BIC.RemoveSpecialCharacters().AddLeadingZeros(4);
			string accNr = AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(10);
			string nationalCheckDigits = NationalCheckDigits.RemoveSpecialCharacters().AddLeadingZeros(2);
			string branchCode = BranchCode.RemoveSpecialCharacters().AddLeadingZeros(4);
			return bic + branchCode + nationalCheckDigits + accNr;
		}
	}
}