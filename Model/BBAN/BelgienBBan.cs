﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.BBAN
{
	public sealed class BelgienBBan : BBanBase
	{
		public override int ValidLength
		{
			get
			{
				return 12;
			}
		}
		public string NationalCheckDigits { get; set; }
		public BelgienBBan()
		{
		}
		public BelgienBBan(string bBan) : this(bBan.Substring(0, 3), bBan.Substring(3, 7), bBan.Substring(10))
		{
		}
		public BelgienBBan(string bic, string accountNumber, string nationalCheckDigits) : base(bic, accountNumber)
		{
			NationalCheckDigits = nationalCheckDigits;
		}
		protected override string AssembleBBan()
		{
			string bic = BIC.RemoveSpecialCharacters().AddLeadingZeros(3);
			string accNr = AccountNumber.RemoveSpecialCharacters().AddLeadingZeros(7);
			string nationalCheckDigits = NationalCheckDigits.RemoveSpecialCharacters().AddLeadingZeros(2);

			return bic + accNr + nationalCheckDigits;
		}
	}
}