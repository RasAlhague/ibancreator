﻿using IbanCreator.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
namespace IbanCreator.UI.Model.BBAN
{
	public abstract class BBanBase : ViewModelBase
	{
		public abstract int ValidLength { get; }
		public string BIC { get; set; }
		public string AccountNumber { get; set; }
		public string BBan { get { return AssembleBBan(); } }
		protected BBanBase()
		{
		}
		protected BBanBase(string bic, string accountNumber)
		{
			BIC = bic;
			AccountNumber = accountNumber;
		}
		protected abstract string AssembleBBan();
	}
}