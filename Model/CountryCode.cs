﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
namespace IbanCreator.UI.Model
{
	public class CountryCode
	{
		public string Code { get; set; }
		public string Country { get; set; }
		public CountryCode(string code, string country)
		{
			Code = code;
			Country = country;
		}
		public static IEnumerable<CountryCode> GetListOfCountryCodes(string filepath)
		{
			string[] lines = File.ReadAllLines(filepath);
			List<CountryCode> countryCodes = new List<CountryCode>();
			foreach (var line in lines)
			{
				countryCodes.Add(new CountryCode(line.Split(';')[0], line.Split(';')[1]));
			}
			return countryCodes;
		}
	}
}