﻿using IbanCreator.UI.Model.BBAN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class GroßbritannienIban : IbanBase
	{
		private GroßbritannienBBan _bBan;
		public GroßbritannienIban() : base(new GroßbritannienBBan())
		{
		}
		public GroßbritannienIban(GroßbritannienBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
        public GroßbritannienIban(params string[] list)
        {
            _bBan = new GroßbritannienBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            _bBan.BranchCode = list[2];
            CheckSum = CalculateChecksum();
        }
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("GB", "Großbritannien");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (GroßbritannienBBan)value;
			}
		}
	}
}