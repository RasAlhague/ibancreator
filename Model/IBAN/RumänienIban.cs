﻿using IbanCreator.UI.Model.BBAN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class RumänienIban : IbanBase
	{
		private RumänienBBan _bBan;
		public RumänienIban() : base(new RumänienBBan())
		{
		}
        public RumänienIban(params string[] list)
        {
            _bBan = new RumänienBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            CheckSum = CalculateChecksum();
        }
		public RumänienIban(RumänienBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("RO", "Rumänien");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (RumänienBBan)value;
			}
		}
	}
}