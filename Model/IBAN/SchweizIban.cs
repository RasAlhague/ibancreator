﻿using IbanCreator.UI.Model.BBAN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class SchweizIban : IbanBase
	{
		private SchweizBBan _bBan;
		public SchweizIban() : base(new SchweizBBan())
		{
		}
		public SchweizIban(SchweizBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
        public SchweizIban(params string[] list)
        {
            _bBan = new SchweizBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            CheckSum = CalculateChecksum();
        }
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("CH", "Schweiz");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (SchweizBBan)value;
			}
		}
	}
}