﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IbanCreator.UI.Model.BBAN;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class FrankreichIban : IbanBase
	{
		private FrankreichBBan _bBan;
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("FR", "Frankreich");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (FrankreichBBan)value;
			}
		}
		public FrankreichIban() : base(new FrankreichBBan())
		{
		}
        public FrankreichIban(params string[] list)
        {
            _bBan = new FrankreichBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            _bBan.BranchCode = list[2];
            _bBan.NationalCheckDigits = list[3];
            CheckSum = CalculateChecksum();
        }
		public FrankreichIban(FrankreichBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
	}
}