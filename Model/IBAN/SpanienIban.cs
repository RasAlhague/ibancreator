﻿using IbanCreator.UI.Model.BBAN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class SpanienIban : IbanBase
	{
		private SpanienBBan _bBan;
		public SpanienIban() : base(new SpanienBBan())
		{
		}
		public SpanienIban(SpanienBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
        public SpanienIban(params string[] list)
        {
            _bBan = new SpanienBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            _bBan.BranchCode = list[2];
            _bBan.NationalCheckDigits = list[3];
            CheckSum = CalculateChecksum();
        }
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("ES", "Spanien");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (SpanienBBan)value;
			}
		}
	}
}