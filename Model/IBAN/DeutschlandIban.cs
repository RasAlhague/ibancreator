﻿using IbanCreator.UI.Model.BBAN;
using System;
using System.Collections.Generic;
using System.Text;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class DeutschlandIban : IbanBase
	{
		private DeutschlandBBan _bBan;
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("DE", "Deutschland");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (DeutschlandBBan)value;
			}
		}
		public DeutschlandIban() : base(new DeutschlandBBan())
		{
		}
        public DeutschlandIban(params string[] list)
        {
            _bBan = new DeutschlandBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            CheckSum = CalculateChecksum();
        }
		public DeutschlandIban(DeutschlandBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
	}
}