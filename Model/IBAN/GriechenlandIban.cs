﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IbanCreator.UI.Model.BBAN;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class GriechenlandIban : IbanBase
	{
		private GriechenlandBBan _bBan;
		public GriechenlandIban() : base(new GriechenlandBBan())
		{
		}
        public GriechenlandIban(params string[] list)
        {
            _bBan = new GriechenlandBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            _bBan.BranchCode = list[2];
            CheckSum = CalculateChecksum();
        }
		public GriechenlandIban(GriechenlandBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("GR", "Griechenland");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (GriechenlandBBan)value;
			}
		}
	}
}