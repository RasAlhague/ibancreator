﻿using IbanCreator.UI.Model.BBAN;
using IbanCreator.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
namespace IbanCreator.UI.Model.IBAN
{
	public abstract class IbanBase : ViewModelBase
	{
		protected int _checkSum;
		protected bool _isValid;
		public abstract CountryCode CountryCode { get; }
		public abstract BBanBase BBan { get; protected set; }
		public string Iban { get { return AssembleIban().SegmentifyString(4); } }
		public bool IsValid
		{
			get
			{
				return _isValid;
			}
			set
			{
				SetProperty<bool>(ref _isValid, value);
			}
		}
		public virtual int CheckSum
		{
			get
			{
				return _checkSum;
			}
			protected set
			{
				SetProperty<int>(ref _checkSum, value);
			}
		}
		protected IbanBase(BBanBase bBan)
		{
			BBan = bBan;
		}
        protected IbanBase(params string[] list)
        {
        }
		protected virtual string AssembleIban()
		{
			return CountryCode.Code + CheckSum.ToString("00") + BBan.BBan;
		}
		protected virtual int CalculateChecksum()
		{
			string ibanWithoutChecksum = BBan.BBan + CountryCode.Code + "00";
            ibanWithoutChecksum = ibanWithoutChecksum.RemoveLetters().RemoveSpecialCharacters();

			BigInteger ibanAsInt = BigInteger.Parse(ibanWithoutChecksum);
			int remaining = (int)(ibanAsInt % 97);
			return 98 - remaining;
		}
        protected virtual void GetIbanData(string iban)
        {
            Type t = Type.GetType("IbanCreator.UI.Model.BBAN." + CountryCode.Country + "BBan");
            BBan = Activator.CreateInstance(t, iban.Substring(4)) as BBanBase;
            CheckSum = Convert.ToInt32(iban.Substring(2, 2));
        }
		public bool ValidateIban()
		{
			if (Iban.Length == BBan.ValidLength + 4)
			{
				string reformedIban = BBan.BBan.RemoveSpecialCharacters() + CountryCode.Code + CheckSum.ToString().AddLeadingZeros(2);
				reformedIban = reformedIban.RemoveLetters();
				BigInteger ibanValue = BigInteger.Parse(reformedIban);
				if (ibanValue % (BigInteger)97 == 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		public static IbanBase CreateIban(string iban)
		{
			string countryCode = iban.Substring(0, 2);
			var countryName = CountryCode.GetListOfCountryCodes(".\\Resources\\CountryCodes.csv").FirstOrDefault(code => code.Code == countryCode);
			if (countryName != null)
			{
				try
				{
					Type t = Type.GetType("IbanCreator.UI.Model.IBAN." + countryName.Country + "Iban");
					var newIban = Activator.CreateInstance(t);
					var concretIban = newIban as IbanBase;
					concretIban.GetIbanData(iban);
					concretIban.ValidateIban();
					return concretIban;
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.Message);
					Debug.WriteLine(ex.Source);
					Debug.WriteLine(ex.StackTrace);
					return null;
				}
			}
			else
			{
				return null;
			}
		}
	}
}