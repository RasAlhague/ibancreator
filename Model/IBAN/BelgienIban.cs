﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IbanCreator.UI.Model.BBAN;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class BelgienIban : IbanBase
	{
		private BelgienBBan _bBan;
        public override CountryCode CountryCode 
        { 
            get 
            { 
                return new CountryCode("BE", "Belgien"); 
            } 
        }
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = value as BelgienBBan;
			}
		}
		public BelgienIban() : base(new BelgienBBan())
		{
		}
        public BelgienIban(params string[] list)
        {
			_bBan = new BelgienBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            _bBan.NationalCheckDigits = list[3];
            CheckSum = CalculateChecksum();
        }
		public BelgienIban(BBanBase bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
	}
}