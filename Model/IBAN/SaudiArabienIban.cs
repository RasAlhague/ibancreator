﻿using IbanCreator.UI.Model.BBAN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IbanCreator.UI.Model.IBAN
{
	public sealed class SaudiArabienIban : IbanBase
	{
		private SaudiArabienBBan _bBan;
		public SaudiArabienIban() : base(new SaudiArabienBBan())
		{
		}
		public SaudiArabienIban(SaudiArabienBBan bBan) : base(bBan)
		{
			CheckSum = CalculateChecksum();
		}
        public SaudiArabienIban(params string[] list)
        {
            _bBan = new SaudiArabienBBan();
            _bBan.AccountNumber = list[0];
            _bBan.BIC = list[1];
            CheckSum = CalculateChecksum();
        }
		public override CountryCode CountryCode
		{
			get
			{
				return new CountryCode("SA", "SaudiArabien");
			}
		}
		public override BBanBase BBan
		{
			get
			{
				return _bBan;
			}
			protected set
			{
				_bBan = (SaudiArabienBBan)value;
			}
		}
	}
}